package musiclibrary;

import java.io.InputStream;

/**
 * Generischer Stream für den Player
 */
public class MusicStream {

    /**
     * Titel des Musik-Stücks
     */
    private String titel = "N / V";

    /**
     * Interpret des Musik-Stücks
     */
    private String interpret = "N / V";

    /**
     * Stream des Musik-Stücks
     */
    private InputStream stream = null;

    /**
     * Gibt den Titel zurück
     * @return
     */
    public String getTitel() {
        return titel;
    }

    /**
     * Setzt den Titel
     * @param titel
     */
    public void setTitel(String titel) {
        this.titel = titel;
    }


    /**
     * Gibt den Interpret zurück
     * @return
     */
    public String getInterpret() {
        return interpret;
    }

    /**
     * Setzt den Interpret
     * @param interpret
     */
    public void setInterpret(String interpret) {
        this.interpret = interpret;
    }

    /**
     * Setzt den Stream
     * @param x
     */
    protected void setStream(InputStream x){
        stream = x;
    }

    /**
     * Gibt den Stream zurück
     * @return
     */
    public InputStream getStream() {
        return stream;
    }
}
