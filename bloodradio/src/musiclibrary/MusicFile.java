package musiclibrary;

import java.io.File;
import java.io.FileInputStream;

/**
 * MusicStream-Abwandlung für eine Datei
 */
public class MusicFile extends MusicStream{

    /**
     * Datei-Objekt
     */
    private File datei = null;

    /**
     * Erstellt ein neues MusicFile-Objekt mit angegebenen Pfad
     * @param fullPath
     */
    public MusicFile(String fullPath){
        this(new File(fullPath));
    }

    /**
     * Erstellt ein neues MusicFile-Objekt mit angegebener Datei
     * @param d
     */
    public MusicFile(File d){
        datei = d;
        try{
            setStream(new FileInputStream(d));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Gibt den Datei-Name zurück
     * @return
     */
    public String getDateiName(){
        return datei.getName();
    }




}
