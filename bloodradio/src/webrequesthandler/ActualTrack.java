package webrequesthandler;

import bloodplayer.Player;
import com.sun.net.httpserver.HttpExchange;
import webinterface.RequestHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Zeigt im Browser die aktuelle Wiedergabe an:
 */
public class ActualTrack extends RequestHandler {

    private Player pl = null;

    public ActualTrack(Player p){
        setContext_name("/actual_track/info");
        pl = p;
    }


    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String response = getDefaultSite();
        response = response.replace("%%TITEL%%",pl.getActualStream().getTitel());
        response = response.replace("%%INTP%%",pl.getActualStream().getInterpret());
        httpExchange.sendResponseHeaders(200,response.getBytes().length);
        try(OutputStream os = httpExchange.getResponseBody()){
            os.write(response.getBytes());
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private String getDefaultSite(){
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <title>Aktuelle Musik-Informationen</title>\n" +
                "        <script src=\"/actual_track/script.js\"></script>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <table>\n" +
                "            <tr>\n" +
                "                <td>Titel:</td>\n" +
                "                <td>%%TITEL%%</td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td>Interpret:</td>\n" +
                "                <td>%%INTP%%</td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td>Aktuelle Position (ms) </td>\n" +
                "                <td id=\"jsPOS\"></td>\n" +
                "            </tr>\n" +
                "        </table>\n" +
                "        <script>\n" +
                "            var source = new EventSource(\"/actual_track/position\");\n" +
                "            source.onmessage = function(event){\n" +
                "                alert(event.data);\n" +
                "            };\n" +
                "        </script>\n" +
                "    </body>\n" +
                "</html>";
    }



}
