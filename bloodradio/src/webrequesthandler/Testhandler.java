package webrequesthandler;

import com.sun.net.httpserver.HttpExchange;
import webinterface.RequestHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Bloodrayne on 15.07.2017.
 */
public class Testhandler extends RequestHandler {

    public Testhandler(){
        setContext_name("/test");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String response = "This is the response";
        httpExchange.sendResponseHeaders(200,response.getBytes().length);
        try(OutputStream os = httpExchange.getResponseBody()){
            os.write(response.getBytes());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
