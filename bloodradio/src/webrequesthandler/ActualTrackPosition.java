package webrequesthandler;

import bloodplayer.Player;
import com.sun.net.httpserver.HttpExchange;
import webinterface.RequestHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Bloodrayne on 15.07.2017.
 */
public class ActualTrackPosition extends RequestHandler {

    private Player pl = null;

    public ActualTrackPosition(Player p){
        setContext_name("/actual_track/position");
        pl = p;
    }


    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        while(true){
            String response = String.valueOf(pl.getCurrentPosition());
            httpExchange.getResponseHeaders().add("Content-Type","text/event-stream");
            httpExchange.getResponseHeaders().add("Cache-Control","no-cache");
            httpExchange.getResponseHeaders().add("Keep-Alive","timeout=5, max=1000");
            httpExchange.sendResponseHeaders(200,response.getBytes().length);
            httpExchange.getResponseBody().write(response.getBytes());
            httpExchange.getResponseBody().flush();
        }

    }
}
