package webrequesthandler;

import com.sun.net.httpserver.HttpExchange;
import webinterface.RequestHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Bloodrayne on 15.07.2017.
 */
public class ActualTrackScript extends RequestHandler {

    public ActualTrackScript(){
        setContext_name("/actual_track/script.js");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String response = getTrackInfoScripte();
        httpExchange.getResponseHeaders().add("Content-Type","text/javascript");
        httpExchange.sendResponseHeaders(200,response.getBytes().length);
        try(OutputStream os=httpExchange.getResponseBody()){
            os.write(response.getBytes());
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private String getTrackInfoScripte(){
        String scr = "function refreshContent(itemid,resp,append){\n" ;
        scr += "\tvar element=document.getElementById(itemid);\n";
        scr += "\tif(append){\n";
        scr += "\t\telement.innerHTML += resp.data;\n";
        scr += "\t}else{\n";
        scr += "\t\telement.innerHTML = resp.data;\n";
        scr += "\t}\n";
        scr += "}";
        return  scr;
    }
}
