package bloodradio;

import bloodplayer.Player;
import musiclibrary.MusicFile;
import webinterface.WebServer;
import webrequesthandler.ActualTrack;
import webrequesthandler.ActualTrackPosition;
import webrequesthandler.ActualTrackScript;
import webrequesthandler.Testhandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Bloodrayne on 14.07.2017.
 */
public class TestKlasse {

    private static Player pl = null;
    private static WebServer srv = null;


    public static void main(String[] args){
        System.out.println("Starte MusicPlayer");
        testMusicPlayer();

        System.out.println("Starte WebServer");
        testWebServer();

        getInput();

        System.out.println("Stoppe Player und Webserver");
        pl.stopPlayer();
        srv.stopServer();
    }


    public static String getInput(){
        try(InputStreamReader is = new InputStreamReader(System.in); BufferedReader rd = new BufferedReader(is)){
            return rd.readLine();
        }catch(Exception e){
            return  "";
        }
    }


    public static void testWebServer(){
        srv = new WebServer();
        srv.prepareServer();
        srv.startServer();
        srv.addHandler(new Testhandler());
        srv.addHandler(new ActualTrackPosition(pl));
        srv.addHandler(new ActualTrack(pl));
        srv.addHandler(new ActualTrackScript());



    }

    public static void testMusicPlayer(){
        //MusicFile mf = new MusicFile("E:\\Musik\\[SNS] • Mad Hatter MEP_HIGH-mc.mp3"); //WINDOWS
        MusicFile mf = new MusicFile("/home/bloodrayne/Musik/Test.mp3"); //LINUX
        mf.setInterpret("SuperNovaStudios");
        mf.setTitel("Mad Hatter");

        pl = new Player();

        ActionListener lst = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e.getActionCommand());
            }
        };

        pl.addListener(lst);
        pl.setTrack(mf);
        pl.prepare();
        pl.play();



    }



}
