package webinterface;

import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

/**
 * Startet einen Webserver auf Port 8080, der die Schnittstelle von außen bilden soll
 */
public class WebServer {

    private HttpServer srv = null;

    public void prepareServer(){
        try{
            srv = HttpServer.create(new InetSocketAddress(8080),0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void startServer(){
        if (srv != null){
            srv.start();
        }
    }

    public void stopServer(){
        if(srv != null){
            srv.stop(0);
        }
    }

    public void addHandler(RequestHandler r){
        if(srv != null){
            srv.createContext(r.getContext_name(),r);
        }
    }

    public void removeHandler(String cntName){
        if(srv != null){
            srv.removeContext(cntName);
        }
    }

}
