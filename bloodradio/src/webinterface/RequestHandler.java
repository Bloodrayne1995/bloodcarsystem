package webinterface;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

/**
 * Created by Bloodrayne on 15.07.2017.
 */
public abstract class RequestHandler implements HttpHandler {

    private String context_name = "";

    public String getContext_name() {
        return context_name;
    }

    public void setContext_name(String context_name) {
        this.context_name = context_name;
    }

    @Override
    public abstract void handle(HttpExchange httpExchange) throws IOException;
}
