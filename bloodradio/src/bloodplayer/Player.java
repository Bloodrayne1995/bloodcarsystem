package bloodplayer;

import musiclibrary.MusicStream;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Wrapper für das Player-Objekt aus javazoom mit Events
 */
public class Player {

    /**
     * Player aus der JavaZoom-Bibliothek
     */
    private javazoom.jl.player.Player abspieler = null;

    /**
     * Event-Listener
     */
    private ArrayList<ActionListener> listeners = new ArrayList<>();

    /**
     * Aktueller Stream, den der Player abspielt bzw. abspielen soll
     */
    private MusicStream current_stream = null;

    /**
     * Abspiel-Thread (aus Vermutung das die Play-Methode vom Player nicht asynchron verläuft)
     */
    private Thread play_thread = null;


    /**
     * Fügt ein Listener zu den Event-Listener hinzu
     * @param x Event-Listener, das hinzugefügt werden soll
     */
    public void addListener(ActionListener x){
        listeners.add(x);
    }

    /**
     * Löst für alle Listener das NextTrack-Event aus
     */
    private void raiseNextTrack(){
        ActionEvent x = new ActionEvent(this,1,"NextTrack");
        for (ActionListener e:
             listeners) {
            e.actionPerformed(x);
        }
    }

    /**
     * Löst für alle Listener das Pause-Event aus
     */
    private void raisePause(){
        ActionEvent x = new ActionEvent(this,2,"Pause");
        for (ActionListener e:
                listeners) {
            e.actionPerformed(x);
        }
    }

    /**
     * Löst für alle Listener das Stop-Event aus
     */
    public void raiseStopped(){
        ActionEvent x = new ActionEvent(this,3,"Stop");
        for (ActionListener e:
                listeners) {
            e.actionPerformed(x);
        }
    }

    /**
     * Löst für alle Listener das Play-Event aus
     */
    public void raisePlay(){
        ActionEvent x = new ActionEvent(this,3,"Play");
        for (ActionListener e:
                listeners) {
            e.actionPerformed(x);
        }
    }

    /**
     * Bereitet den Player vor, sodass er nur noch gestartet werden muss
     */
    public void prepare(){
        if(abspieler == null){
            try{
                abspieler = new javazoom.jl.player.Player(current_stream.getStream());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }




    /**
     * Startet den Player in einem separatem Thread
     */
    public void play(){
        if(abspieler != null){

            play_thread = new Thread(){
                @Override
                public void run() {
                    try{
                        abspieler.play();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            };


            play_thread.start();
            raisePlay();
        }
    }

    /**
     * Stoppt den Player
     */
    public void stopPlayer(){
        if(abspieler != null){
            abspieler.close();
            raiseStopped();
        }
    }

    /**
     * Setzt den Stream, den der Player abspielen soll
     * @param d Neuer Stream
     */
    public void setTrack(MusicStream d){
        current_stream = d;
        raiseNextTrack();
    }

    /**
     * Gibt die aktuelle Position in MS zurück
     * @return
     */
    public int getCurrentPosition(){
        if(abspieler != null){
            return  abspieler.getPosition();
        }else{
            return 0;
        }
    }


    public MusicStream getActualStream(){
        return current_stream;
    }






}
